name := "scalasyd-doobie"

version := "1.0"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "tpolecat" at "http://dl.bintray.com/tpolecat/maven",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

val http4sVersion = "0.11.2"

libraryDependencies ++= Seq(
  "org.http4s"              %%        "http4s-blaze-server"  % http4sVersion,
  "org.http4s"              %%        "http4s-dsl"           % http4sVersion,
  "org.http4s"              %%        "http4s-argonaut"      % http4sVersion,
  "org.tpolecat"            %%        "doobie-core"          % "0.2.3",
  "org.postgresql"           %        "postgresql"           % "9.2-1004-jdbc41",
  "joda-time"                %        "joda-time"            % "2.7",
  "org.joda"                 %        "joda-convert"         % "1.8",
  "io.argonaut"             %%        "argonaut"             % "6.1",
  "net.databinder.dispatch" %%        "dispatch-core"        % "0.11.2",
  "ch.qos.logback"           %        "logback-classic"      % "1.1.2"
)
