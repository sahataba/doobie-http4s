package scalasyd

import argonaut._
import Argonaut._
import org.joda.time.LocalDate
import org.joda.time.DateTimeZone
import scalaz.stream._

object Json {

  def codec[A: EncodeJson: DecodeJson] = CodecJson.derived[A]

  implicit val artistIdJson: CodecJson[Artist.Id] = codec[Long].xmap(Artist.Id(_))(_.id)
  implicit val artistJson: CodecJson[Artist.Info] = codec[String].xmap(Artist.Info(_))(_.name)
  implicit val bandIdJson: CodecJson[Band.Id] = codec[Long].xmap(Band.Id(_))(_.id)
  implicit val localDateJson: CodecJson[LocalDate] =
    codec[Long].xmap(new LocalDate(_))(_.toDateTimeAtStartOfDay(DateTimeZone.UTC).getMillis)
  implicit val bandJson: CodecJson[Band.Info] = casecodec2(Band.Info.apply, Band.Info.unapply)("name", "started")

  implicit val artistViewJson: CodecJson[Artist.View] = casecodec2(Artist.View.apply, Artist.View.unapply)("id", "name")
  implicit val bandViewJson: CodecJson[Band.View] = casecodec2(Band.View.apply, Band.View.unapply)("id", "band")
  implicit val bandArtistViewJson: CodecJson[BandArtistView] =
    casecodec2(BandArtistView.apply, BandArtistView.unapply)("band", "artists")

  implicit class AddJsonToProcess[M[_], A: CodecJson](as: Process[M, A]) {
    import Process._

    def asJsonArray: Process[M, String] =
      emit("[") ++ as.map(_.asJson.nospaces).intersperse(",") ++ emit("]")
  }
}
