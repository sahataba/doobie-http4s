package scalasyd

import org.joda.time.LocalDate

object Band {
  case class Id(id: Long)
  case class Info(name: String, started: LocalDate)
  case class View(id: Id, band: Info)
}

object Artist {
  case class Id(id: Long)
  case class Info(name: String)
  case class View(id: Id, artist: Info)
}


case class BandArtistView(band: Band.View, artists: Vector[Artist.View])
