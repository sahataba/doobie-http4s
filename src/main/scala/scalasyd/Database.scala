package scalasyd

import doobie.imports._
import scalaz._
import Scalaz._
import scalaz.concurrent.Task
import scalaz.stream._
import java.sql.{Date => SqlDate}
import org.joda.time.LocalDate
import org.joda.time.DateTimeZone
import Json._

object Database {

  val tx = DriverManagerTransactor[Task]("org.postgresql.Driver", "jdbc:postgresql:scalasyd", "postgres", "postgres")

  implicit val localDateMeta: Meta[LocalDate] =
    Meta[SqlDate].xmap(
      s => new LocalDate(s.getTime),
      l => new SqlDate(l.toDateTimeAtStartOfDay(DateTimeZone.UTC).getMillis)
    )

  def create(artist: Artist.Info, band: Band.Info): Process[ConnectionIO, (Artist.Id, Band.Id)] =
    for {
      aid <- create(artist)
      bid <- create(band)
      _   <- link(aid, bid)
    } yield (aid, bid)

  def create(artist: Artist.Info): Process[ConnectionIO, Artist.Id] =
    sql"INSERT INTO artists (name) VALUES (${artist.name})".update.withGeneratedKeys[Artist.Id]("id")

  def create(band: Band.Info): Process[ConnectionIO, Band.Id] =
    sql"INSERT INTO bands (name, started) VALUES (${band.name}, ${band.started})".update.withGeneratedKeys[Band.Id]("id")

  def link(artist: Artist.Id, band: Band.Id): Process[ConnectionIO, Int] =
    Process.eval {
      sql"INSERT INTO bands_artists (artist_id, band_id) VALUES (${artist.id}, ${band.id})".update.run
    }

  case class BandArtistJoin(bid: Band.Id, b: Band.Info, aid: Artist.Id, a: Artist.Info)

  def findBandsWithArtists =
    sql"""
      SELECT b.id, b.name, b.started, a.id, a.name
      FROM bands b
      INNER JOIN bands_artists ba ON (b.id = ba.band_id)
      INNER JOIN artists a ON (a.id = ba.artist_id)
      ORDER BY b.id
    """.query[BandArtistJoin].process.chunkBy2 { _.bid == _.bid }.map(groupToBands)

  def groupToBands(bas: Vector[BandArtistJoin]): BandArtistView =
    BandArtistView(
      Band.View(bas.head.bid, bas.head.b),
      bas.map { b =>
        Artist.View(b.aid, b.a)
      }
    )
}
