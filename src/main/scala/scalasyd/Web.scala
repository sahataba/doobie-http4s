package scalasyd

import org.http4s.server.HttpService
import org.http4s.server.blaze.BlazeBuilder
import org.http4s.dsl._
import org.http4s.argonaut._
import org.http4s.EntityDecoder
import doobie.imports._
import argonaut._
import Argonaut._
import Json._

object Web {

  implicit val bandInfoDecoder: EntityDecoder[Band.Info] = jsonOf[Band.Info]

  val route = HttpService {

    case GET -> Root / "bands" / "artists" =>
      Ok {
        val a = Database.findBandsWithArtists.transact(Database.tx)
        a.asJsonArray
      }

    case req @ PUT -> Root / "band" =>
      req.decode[Band.Info] { b =>
        Ok {
          Database.create(b).transact(Database.tx).map(_.asJson.nospaces)
        }
      }
  }

  val service: HttpService = route

  def main(args: Array[String]): Unit =
    BlazeBuilder.bindHttp(8080)
      .mountService(service, "/")
      .run
      .awaitShutdown()

}
